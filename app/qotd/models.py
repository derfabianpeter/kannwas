# Import the database object (db) from the main application module
# We will define this inside /app/__init__.py in the next sections.
from app import db

# Define a base model for other database tables to inherit
class Base(db.Model):

    __abstract__  = True

    id            = db.Column(db.Integer, primary_key=True)
    date_created  = db.Column(db.DateTime,  default=db.func.current_timestamp())
    date_modified = db.Column(db.DateTime,  default=db.func.current_timestamp(),
                                           onupdate=db.func.current_timestamp())

# Define a User model
class Quote(Base):

    __tablename__ = 'quotes'

    quote    = db.Column(db.String(512),  nullable=False)
    author   = db.Column(db.String(128),  nullable=True)
    background = db.Column(db.String(512),  nullable=True)

    # Authorisation Data: role & status
    user     = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    status   = db.Column(db.SmallInteger, nullable=False)

    # New instance instantiation procedure
    def __init__(self, quote):

        self.quote     = quote

    def __repr__(self):
        return '<%r>' % (self.quote)
