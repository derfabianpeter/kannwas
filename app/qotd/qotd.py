#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
import random
from PIL import Image, ImageFont, ImageDraw
from app import db
from app import app
import giphy_client
from giphy_client.rest import ApiException
import json
from pprint import pprint

class QOTD:
    quotes = [
        {"quote": "If you do not create and control your environment, your environment creates and controls you", "bg": None},
        {"quote": "The best way to predict your future is to create it", "bg": None},
        {"quote": "Every next level of your life will require a different you", "bg": None},
        {"quote": "What got you here won’t get you there", "bg": None},
        {"quote": "Whatever the mind can conceive and believe, it can achieve", "bg": None},
        {"quote": "Was ist von unseren Träumen übrig geblieben? 100% wir haben nicht übertrieben", "bg": None},
        {"quote": "Wir können den Wind nicht ändern, aber die Segel anders setzen", "bg": None},
        {"quote": "Be the change you wish to see in the world", "bg": None},
        {"quote": "Yesterday I was clever, so I wanted to change the world. Today I am wise, so I am changing myself", "bg": None},
        {"quote": "Say no to more dumb shit. Engage with fewer things but at a higher intensity. Stick with it. Stop chasing so much", "bg": None},
        {"quote": "Hustle with heart, Be the change, Make a difference", "bg": None},
        {"quote": "Passion is a result, an Energy. Passion is the feeling you have when you are engaged in something you‘d do for free and can‘t believe you're getting paid for it", "bg": None},
        {"quote": "If you want to go quickly, go alone. If you want to go far, go with others", "bg": None},
        {"quote": "I ask potential business partners, 'Do you love the business, or do you love the money?' I only want to work with people who love the business", "bg": None},
        {"quote": "Criticism may not be agreeable, but it is necessary. It fulfils the same function as pain in the human body. It calls attention to an unhealthy state of things", "bg": None},
        {"quote": "You can do anything, but you can’t do everything", "bg": None},
        {"quote": "So often people are working hard at the wrong thing. Working on the right thing is probably more important than working hard", "bg": None},
        {"quote": "If you can't explain something in simple terms, you don't understand it", "bg": None},
        {"quote": "What I cannot create, I do not understand", "bg": None},
        {"quote": "But, sooner or later, all of us have to ask ourselves what our mission is — is it to be the busiest or is it to make the most impact?", "bg": None},
        {"quote": "you can’t have hope for the future if you don’t believe you can change", "bg": None},
        {"quote": "Your future should always be bigger than your past", "bg": None},
        {"quote": "The horizon always moves, no matter where you are. It’s the direction, not the destination", "bg": None},
        {"quote": "We’re bad at most things by default. The only way to overcome the deficit is with the right kind of practice.", "bg": None},
        {"quote": "the only way to eliminate surprise is to have seen it all before", "bg": None}
    ]

    api_instance = None
    api_key = 'xJIC7txyevLmWcq36nFELXwul4NyBAxS' # str | Giphy API Key.
    tag = 'motivation' # str | Search query term or prhase.
    limit = 25 # int | The maximum number of records to return. (optional) (default to 25)
    offset = 0 # int | An optional results offset. Defaults to 0. (optional) (default to 0)
    rating = 'g' # str | Filters results by specified rating. (optional)
    lang = 'en' # str | Specify default country for regional content; use a 2-letter ISO 639-1 country code. See list of supported languages <a href = \"../language-support\">here</a>. (optional)
    fmt = 'json' # str | Used to indicate the expected response format. Default is Json. (optional) (default to json)

    def __init__(self):
        self.api_instance = giphy_client.DefaultApi()
        self.img_width = app.config['IMAGE_WIDTH']
        self.img_height = app.config['IMAGE_HEIGHT']
        self.bg_color = self.getHex()
        self.text_color = app.config['IMAGE_TEXT_COLOR']
        self.text_size = app.config['IMAGE_TEXT_SIZE']
        print(app.config['IMAGE_TEXT_FONT'])
        self.font = ImageFont.truetype(app.config['IMAGE_TEXT_FONT'], self.text_size)
        self.img = Image.new('RGB', (self.img_width, self.img_height), self.bg_color)
        self.draw = ImageDraw.Draw(self.img)

    def getHex(self):
        r = lambda: random.randint(0, 255)
        return '#%02X%02X%02X' % (r(), r(), r())

    def getComplementary(self,my_hex):
        """Returns complementary RGB color

        Example:
        >>>complementaryColor('FFFFFF')
        '000000'
        """
        if my_hex[0] == '#':
            my_hex = my_hex[1:]
        rgb = (my_hex[0:2], my_hex[2:4], my_hex[4:6])
        comp = ['%02X' % (255 - int(a, 16)) for a in rgb]
        return '#'+''.join(comp)


    def renderImage(self, quote):
        quote = quote.upper()
        sum = 0
        for letter in quote:
          sum += self.draw.textsize(letter, font=self.font)[0]

        average_length_of_letter = sum/len(quote)

        number_of_letters_for_each_line = (self.img_width/1.618)/average_length_of_letter
        incrementer = 0
        fresh_sentence = ''

        for letter in quote:
          if(letter in ['-', '.', '?', '!']):
            fresh_sentence += letter + '\n\n'
          elif(incrementer < number_of_letters_for_each_line):
            fresh_sentence += letter
          else:
            if(letter in [' ', ',']):
              fresh_sentence += '\n'
              incrementer = 0
            else:
              fresh_sentence += letter
          incrementer+=1

        print(fresh_sentence)

        #render the text in the center of the box
        dim = self.draw.textsize(fresh_sentence, font=self.font)
        x2 = dim[0]
        y2 = dim[1]

        qx = (self.img_width/2 - x2/2)
        qy = (self.img_height/2-y2/2)

        self.draw.text((qx,qy), fresh_sentence , align="center",  font=self.font, fill=(255,255,255))

        return self.img

    def getGif(self):
        try:
            # Search Endpoint
            api_response = self.api_instance.gifs_random_get(self.api_key, tag=self.tag, rating=self.rating, fmt=self.fmt)
            r = api_response.data.image_url
            return r
        except ApiException as e:
            print("Exception when calling DefaultApi->gifs_search_get: %s\n" % e)

    def quote(self,tag=None):
        quote = random.choice(self.quotes)
        quote['bg'] = self.getGif()
        return quote
