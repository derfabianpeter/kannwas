# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for

# Import password / encryption helper tools
from werkzeug import check_password_hash, generate_password_hash

# Import the database object from the main app module
from app import db
from app import app
import random

from app.qotd.qotd import QOTD
from io import BytesIO
from flask import send_file
import os

# Define the blueprint: 'auth', set its url prefix: app.url/auth
qotd = Blueprint('qotd', __name__, url_prefix='/qotd')

@qotd.route('/', methods=['GET'])
def index():
    qotd = QOTD()
    quote = qotd.quote()
    return render_template("qotd/index.html", bgcolor=qotd.getHex(), highlight_color=qotd.getHex(), quote=quote)

@qotd.route('/logo/', methods=['GET'])
def logo():
    return render_template("qotd/logo.html")

# Set the route and accepted methods
@qotd.route('/random/', methods=['GET'])
def random_quote():
    # blah blah here
    #return render_template("qotd/random.html")
    qotd = QOTD()
    #img = qotd.prepareString("Everybody is a genius. But if you judge a fish by its ability to climb a tree, it will live its whole life believing that it is stupid. -Albert Einstein")
    img = qotd.renderImage("Was ist von unseren Träumen übrig geblieben? 100% wir haben nicht übertrieben!")
    #img = qotd.renderImage("Everybody is a genius. But if you judge a fish by its ability to climb a tree, it will live its whole life believing that it is stupid.")

    byte_io = BytesIO()
    img.save(byte_io, 'PNG')
    byte_io.seek(0)
    return send_file(byte_io, mimetype='image/png')

@qotd.route('/calendar/', methods=['GET'])
def calendar():
    return render_template("qotd/calendar.html")
