# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for

# Import password / encryption helper tools
from werkzeug import check_password_hash, generate_password_hash

# Import the database object from the main app module
from app import db

from app.wzkino.wzkino import News
import os
import random

# Define the blueprint: 'auth', set its url prefix: app.url/auth
wzkino = Blueprint('wzkino', __name__, url_prefix='/wzkino')

# Set the route and accepted methods
@wzkino.route('/random', methods=['GET'])
def random_news():
    rss = News()
    feed_items = rss.parse("http://www.faz.net/rss/aktuell/")
    item = random.choice(feed_items)
    return render_template("wzkino/random.html", item=item)

@wzkino.route('/news/', methods=['GET'])
def news():
    rss = News()
    feed_items = rss.parse("http://www.faz.net/rss/aktuell/")
    item = random.choice(feed_items)
    template = os.getenv("TEMPLATE","default")

    feedItemCounter = 0
    htmlPageCounter = 0
    items = []

    for item in feed_items:
        items.append(item)
        print(item)
        feedItemCounter += 1

        if feedItemCounter % 4 == 0:
            htmlPageCounter += 1
            pageName = 'page' + str(htmlPageCounter) + '.html'
            #path = basePath + "news/" + pageName
            #newsHTML = rss.dash(items,template)

            return render_template("wzkino/news.html", feed_items=items)

            items = []
