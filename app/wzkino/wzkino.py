#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
import random
from app import app
import feedparser
from jinja2 import Environment, FileSystemLoader
import os

class News:
    def __init__(self):
        self._dir = os.path.dirname(os.path.abspath(__file__))

    def parse(self,feed_url=None):
        if feed_url:
            data = feedparser.parse(feed_url)
            feed_items = []

            for entry in data.entries:
                description = entry.description.replace("article_teaser/","")
                description = description.replace('height=\"auto\"','height=\"auto\"')
                description = description.replace('width=\"auto\"','width=\"auto\"')
                feed_item = {
                    "title": entry.title,
                    "description": description,
                }
                feed_items.append(feed_item)

            self._parse = feed_items
            return feed_items

    def dash(self,feed_items=None,template="default"):
        template_path = "templates/%s" % template
        # Create the jinja2 environment.
        # Notice the use of trim_blocks, which greatly helps control whitespace.
        j2_env = Environment(loader=FileSystemLoader(self._dir),
                             trim_blocks=True)
        return j2_env.get_template('%s/index.html' % template_path).render(
            feed_items=feed_items
    )

    def singleItem(self,item=None,template="default"):
        template_path = "templates/%s" % template
        # Create the jinja2 environment.
        # Notice the use of trim_blocks, which greatly helps control whitespace.
        j2_env = Environment(loader=FileSystemLoader(self._dir),
                             trim_blocks=True)
        return j2_env.get_template('%s/index.html' % template_path).render(
            item=item
    )
