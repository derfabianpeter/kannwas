#!/usr/bin/python3
# -*- coding: utf-8 -*-
import sys
import os
import logging
import uuid
import random
import json
from PIL import Image, ImageFont, ImageDraw
from flask import Flask, abort, send_file
from io import BytesIO
from flask_prometheus import monitor
#from raven import Client
from raven.contrib.flask import Sentry


#client = Client('https://8fa90b2b0eb34ad18c13b9f74b3a6d1f:1ad9e6f8ce7a4cd1b6fa0f833fce7f78@sentry.meta.munchiez.io/2')

app = Flask(__name__)
sentry = Sentry(app, logging=True, level=logging.ERROR, dsn='https://8fa90b2b0eb34ad18c13b9f74b3a6d1f:1ad9e6f8ce7a4cd1b6fa0f833fce7f78@sentry.meta.munchiez.io/2?verify_ssl=0')
asset_path = os.getenv("ASSET_PATH", "assets")
offset = 20

W = 1920
H = 1080

string_path = os.getenv("STRINGS", "strings.json")
strings = []
try:
    with open(string_path) as f:
        strings = json.load(f)
except Exception as e:
    sentry.captureException(e)

def getFonts():
    fonts = []
    fonts_path = asset_path+"/fonts"
    if os.path.isdir(fonts_path):
        font_assets = os.listdir(fonts_path)
        for font in font_assets:
            font_path = fonts_path+"/"+font
            if os.path.isdir(font_path) and not font.startswith("."):
                styles = os.listdir(font_path)
                f = {
                    "name": font
                }
                for style in styles:
                    res = os.path.splitext(style)[0]
                    if res.startswith("."):
                        continue
                    if res == "OFL":
                        continue
                    n, s = res.split("-")
                    f[s] = fonts_path+"/"+font+"/"+style

                fonts.append(f)

    return fonts


def pickFont():
    fonts = getFonts()
    return fonts[random.randint(0, len(fonts)-1)]


def getHex():
    r = lambda: random.randint(0, 255)
    return '#%02X%02X%02X' % (r(), r(), r())


def getComplementary(my_hex):
    """Returns complementary RGB color

    Example:
    >>>complementaryColor('FFFFFF')
    '000000'
    """
    if my_hex[0] == '#':
        my_hex = my_hex[1:]
    rgb = (my_hex[0:2], my_hex[2:4], my_hex[4:6])
    comp = ['%02X' % (255 - int(a, 16)) for a in rgb]
    return '#'+''.join(comp)


def drawText(msg, pos, img, draw):
    fontSize = 72;
    lines = []

    f = pickFont()['Regular']
    if pos == "top":
        f = pickFont()['Bold']


    font = ImageFont.truetype(f, fontSize)
    w, h = draw.textsize(msg, font)

    imgWidthWithPadding = img.width * 0.90

    # 1. how many lines for the msg to fit ?
    lineCount = 1
    if(w > imgWidthWithPadding):
        lineCount = int(round((w / imgWidthWithPadding) + 1))

    if lineCount > 2:
        while 1:
            fontSize -= 2
            font = ImageFont.truetype(f, fontSize)
            w, h = draw.textsize(msg, font)
            lineCount = int(round((w / imgWidthWithPadding) + 1))
            app.logger.info("try again with fontSize={} => {}".format(fontSize, lineCount))
            if lineCount < 3 or fontSize < 10:
                break


    app.logger.info("img.width: {}, text width: {}".format(img.width, w))
    app.logger.info("Text length: {}".format(len(msg)))
    app.logger.info("Lines: {}".format(lineCount))


    # 2. divide text in X lines
    lastCut = 0
    isLast = False
    for i in range(0,lineCount):
        if lastCut == 0:
            cut = (len(msg) // lineCount) * i
        else:
            cut = lastCut

        if i < lineCount-1:
            nextCut = (len(msg) // lineCount) * (i+1)
        else:
            nextCut = len(msg)
            isLast = True

        app.logger.info("cut: {} -> {}".format(cut, nextCut))

        # make sure we don't cut words in half
        if nextCut == len(msg) or msg[nextCut] == " ":
            app.logger.info("may cut")
        else:
            app.logger.info("may not cut")
            while msg[nextCut] != " ":
                nextCut += 1
            print("new cut: {}".format(nextCut))

        line = msg[cut:nextCut].strip()

        # is line still fitting ?
        w, h = draw.textsize(line, font)
        if not isLast and w > imgWidthWithPadding:
            app.logger.info("overshot")
            nextCut -= 1
            while msg[nextCut] != " ":
                nextCut -= 1
            print("new cut: {}".format(nextCut))

        lastCut = nextCut
        lines.append(msg[cut:nextCut].strip())

    # 3. print each line centered
    lastY = H/2-(h*lineCount)-offset
    app.logger.error("Test Error")
    if pos == "bottom":
        # lastY = img.height - h * (lineCount+1) - (img.height/3)
        lastY = H/2+offset

    for i in range(0,lineCount):
        w, h = draw.textsize(lines[i], font)
        textX = img.width/2 - w/2
        #if pos == "top":
        #    textY = h * i
        #else:
        #    textY = img.height - h * i
        textY = lastY + h
        draw.text((textX, textY),lines[i],(255,255,255),font=font)
        lastY = textY


    return

def createImage(string):
    color = getHex()
    comp = "#ffffff"
    img = Image.new('RGB', (W, H), color)
    draw = ImageDraw.Draw(img)

    drawText(string[0].upper(), "top", img, draw)

    if len(string) > 1:
        drawText(string[1].upper(), "bottom", img, draw)

    #img.save(motd_path+"/"+str(uuid.uuid4())+".png")
    return img

def getRandom():
    string = random.choice(strings)
    img = createImage(string)
    return img

@app.route('/random')
def get_random():
    img_io = BytesIO()
    img = getRandom()
    img.save(img_io, 'PNG')
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')

# if __name__ == "__main__":
#     for string in strings:
#
#         color = getHex()
#         comp = "#ffffff"
#         W = 1920
#         H = 1080
#         img = Image.new('RGB', (W, H), color)
#         draw = ImageDraw.Draw(img)
#
#         drawText(string[0].upper(), "top")
#
#         if len(string) > 1:
#             drawText(string[1].upper(), "bottom")
#
#         img.save(motd_path+"/"+str(uuid.uuid4())+".png")
#     sys.exit(0)
if __name__ == '__main__':
    #monitor(app, port=5002)
    app.run()
