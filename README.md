# Install

```
git clone git@gitlab.munchiez.io:fabian/kannwas.git
pip3 install virtualenv
pip3 install python-dotenv
cd kannwas
virtualenv venv
. venv/bin/activate
pip install -r requirements.txt
flask run
```

# Use
Visit http://127.0.0.1:5000/wzkino/news 
