FROM python:3.7.0
LABEL maintainer="Fabian Peter <fabian@munchiez.io>"

WORKDIR /app

COPY . .
RUN pip3 install -r requirements.txt
ENV TZ=Europe/Berlin

EXPOSE 5000
CMD python3 run.py
